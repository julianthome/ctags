#!/bin/sh

# For macOS
#CC="/usr/local/bin/gcc-10"

CC="gcc"
CFLAGS="-fPIC"

export CC=${CC}
export CFLAGS=${CFLAGS} 

INCLUDES="/usr/local/include/ctags"
LIBDIR="/usr/local/lib/"

mkdir -p "$INCLUDES"
mkdir -p "$LIBDIR"

# build ctags
./autogen.sh
./configure 
make install


find main -name "*.h" -exec cp "{}" "$INCLUDES" \;
cp libctags.a /usr/local/lib/

exit 0
